---
title: Meine Wohnung
layout: post
lang: de
category: /blog/de/
---
Seit März war ich, wegen dem Coronavirus, in meinem Heimatort, aber diese Woche
bin ich zurück nach Austin gefahren, wo meine Uni ist. Es war langweilig in
meinem Heimatort und ich bin froh, dass ich endlich wieder in meiner Wohnung
bin. Trotzdem vermisse ich meine Mutter, meinen Bruder und natürlich auch meine
Katze Dave! In Austin bin ich nur allein und einsam gewesen. Vielleicht
schmecken die Kirschen in Nachbars Garten wirklich immer ein bisschen süßer.

Morgen kommt meine Freundin zurück nach Austin und ich muss ihr helfen ihre neue
Wohnung zu beziehen. Es ist das erste mal das sie alleine wohnt. Sie freut sich
sehr, aber ist natürlich auch ein bisschen nervös.

