---
title: My site's move to Jekyll
layout: post
lang: en
category: /blog/en
---
A few days ago I decided to move my website to use Jekyll instead of the basic
HTML that I had been using. Jekyll adds a bit of complexity to setting up my deve
lopment environment but it's really nice to get themes & some of the other
features of Jekyll.

One of the big things that I like is that I can easily write my blog posts in
seperate files, with the date in the filename, and easily have them added to the
list of blog posts. Before, I was writing every post in a single HTML file and
having to manually put the dates as well as all sorts of formatting. Now, they
can be in seperate files and easily added to a list of all blog posts on the
respective English/German blog pages. I can also write my posts in markdown rather
than HTML now. While not super important, it's a nice quality of life
improvement.

It's pretty nice being able to have predefined layouts, too. Layouts are just
HTML files that can be used by the various markdown pages. It makes it really
easy to add new pages (like blog posts) without having to copy over all of the
HTML for every new entry. Layouts can also be used to automatically create the
URL for a webpage, which makes things simpler for me.

The only issue that I've had with my transition to Jekyll was trying to figure
out how to set up my German blog posts to use the German date format (and month
names). Eventually, though, I started to get the hang of it and found a website
with an example already written with the German months! I got that set up and
now my English and German post layouts each have their own date format that
works well.

I'll probably try to make my site a little bit nicer looking in the future,
maybe with a better theme, and try to add some more detailed information on the
home and about pages.

