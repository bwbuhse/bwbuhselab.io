---
title: Ein paar Fakten zu Schaffhausen
layout: post
lang: de
category: /blog/de
---
Ich bin Amerikaner, aber ich möchte sehr gerne nach Deutschland und die Schweiz
reisen. Als ich jünger war, wohnte ich in der Schweiz, daher liebe und vermisse
ich beide Länder.

In der Schweiz wohnte ich in Diessenhofen bei Schaffhausen. Ich glaube,
Schaffhausen die einzige Schweizer Stadt nördlich vom Rhein. Während des Zweiten
Weltkriegs haben die USA den Bahnhof von Schaffhausen bombardiert.

Schaffhausen ist auch in der Nähe vom Rheinfall! Der Rheinfall ist der breiteste
Wasserfall in Europa. Ich finde ihn sehr schön. Vielleicht, wenn der Coronavirus
vorbei ist, kann ich zurück gehen und ihn besuchen.

Noch ein interessanter Fakt zu Schaffhausen ist, dass sie eine Festung namens
der Munot hat. Sie wurde im 16. Jahrhundert erbaut! Man kann im Munot gehen und
ihre Geschichte lernen und die Aussicht von der Stadt genießen.
