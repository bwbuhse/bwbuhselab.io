---
title: Updates to Dotboy
layout: post
lang: en
category: /blog/en/
---
I've been updating my script for dot file repository management and ended up
renaming it to DotBoy, since dotman already exists on PyPi. I've added a few
features but my favorite things I've done have been publishing it to PyPi and
put packages for it on the AUR!
