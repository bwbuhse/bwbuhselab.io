---
title: Der Semester ist fast Vorbei
layout: post
lang: de
category: /blog/de
---
Der Semester ist nach der nächsten Woche vorbei. Dieses ist mein sechstes
Semester in der Uni. Ich gehe zur University of Texas at Austin. Ich habe noch
zwei Semester, bevor ich meinen Abschluss mache. Ich studiere Elektro- und
Computertechnik, weil ich ein Programmierer werden möchte. Ich finde
Programmieren sehr interessant und find, dass es viel Spaß macht.
