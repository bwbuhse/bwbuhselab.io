---
title: Internship Week 1
layout: post
lang: en
category: /blog/en/
---
This past Monday I started my internship for this year. I'm working as a
research and development intern at Trend Micro Tipping Point in Austin. It's
actually my second year at the company, last summer I interned there as well,
but I'm on a different team now. I'm going to working on writing and updating
unit tests for their
[IPS](https://www.trendmicro.com/en_us/business/products/network/intrusion-prevention.html)
boxes. I'm really excited to be working on this project, especially because
it'll be my first time really getting a look at what it's like to work on an
existing code base. Last summer I just worked on a project with some other
interns where we created an internal web app which was very fun, but we
basically started from scratch and didn't do much work on existing code.
