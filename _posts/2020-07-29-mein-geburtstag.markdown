---
title: Mein Geburtstag
category: /blog/de
lang: de
layout: post
---
Gestern bin ich 21 geworden. Ich wohne in den USA, und kann jetzt endlich
Alkohol kaufen! Ich habe nur mit meiner Mutter, Oma, und Freundin zu Abend
gegessen, weil Coronavirus aktuell sehr verbreitet ist.

Ich habe von 9 Uhr bis 15 Uhr gearbeitet, danach bin ich den Schnapsladen
gegangen. Ich habe Rotwein und ein paar „Truly” gekauft. Danach habe ich mit
meiner Familie eine meterlange Pizza gegessen. Es war keine große Party, aber
wir haben viel Spaß gehabt.
