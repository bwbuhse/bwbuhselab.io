---
title: I finally went to get my desktop!
layout: post
lang: en
category: /blog/en/
---
My spring break from college started right before COVID-19 became a true
pandemic. Because of this, I had left my desktop in Austin at my apartment, but
then I didn't have it with me at home in Dallas since I wasn't planning on going
back for a while. Well, I finally got it back since I had to go get some clothes
and other things I left in Austin anyways!

Since I finally have my desktop again, I figured that it'd be fun to mess around
with my laptop's OS some more since it's not too important if I mess it up for a
few days anymore. I started off by trying to re-install Arch with full-disk
encryption which... failed. I got a bit confused, and ended up decided not to do
that. Instead, I did try out btrfs instead of ext4! I got that set-up and
working fine, which was fun, but I needed something else to do. That's when I
decided to try and install set-up the openssh server on my laptop so that I can
ssh in from my desktop and edit this site (or other files, I guess). It's not too
big of a difference, but it's nice to be able to use my mechanical keyboard and
nice monitor without having to detach them from my desktop just to program a
little bit. It was surprisingly easy, too. Now I just need to work on setting up
ssh keys, so that I don't have to type my password every time I log in. After
that I'll probably try figuring out how to connect to my laptop via its host
name or, at the very least, setting a static ip on my laptop so that I won't
have to change the ssh config on my desktop. For now, I think it's a good
solution that works well.

p.s. I know this is pretty pointless to talk about, but it was a fun little
thing to do and just took me a few minutes so I decided that I might as well
write about it.

