---
title: Meine Katze
layout: post
category: /blog/de/
lang: de
---
Ich habe letzte Woche eine neue Katze adoptiert. Er heißt Dave, kurz für
Milkshake Dave, und er ist fünf Jahre alt. Er hat zu einem Freund von meiner
Mutter gehört, aber er ist im Februar verheiratet geworden und seine Frau ist
gegen Katze allergisch. Dave ist noch scheu, aber ich hoffe, dass er sich bald
an mein Haus gewöhnen werden.

Dave ist sehr schön! Er ist weiß mit grauen Flecken auf seiner Hüfte. Seine
Augen sind grün, aber sie haben einen blauen Schimmer im Licht. Abends kuschelt
er mit mir und schnurrt sehr laut. Er mag meinen Hund noch nicht :(
