---
layout: page
order: 3
title: About
permalink: /about/
---

I'm a ECE student at UT Austin planning to graduate in 2021. I was born in
Austin, but lived in Ireland, Switzerland, and some various suburbs of Dallas
before coming back to Austin for college. I'm a big a hockey fan (go Stars!)
and I also have dual-citizenship with Canada. In the future, I'd love to live
in Europe again, but if I can't make it there, somewhere in Canada or the Northern
US would be nice too.

I mostly use this site for blogging about random topics I find interesting. Thanks
for stopping by!
